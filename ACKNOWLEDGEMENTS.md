# Acknowledgements

We are grateful to the following individuals and resources for inspiration and contributions to this repository.

## Direct contributions

Contributors are listed alphabetically by surname, following [CRediT (Contributor Roles Taxonomy)](https://casrai.org/credit/) notation.

Please see the full [credit listing](./docs/credit.csv) for a list of all contributors.

## Inspiration and resources

Inspired by Huijser, Dorien, Achterberg, Michelle, Wierenga, Lara, Van't Veer, Anna, Klapwijk, Eduard, Van Erkel, Raymond, & Hettne, Kristina. (2020, June 19). MRI data sharing guide. Zenodo.  http://doi.org/10.5281/zenodo.3822290

Created using materials from Das et al, 2019, [MCIN Open Science Guidance: Data Preparation Checklist](https://loris.ca/MCINOpenScienceGuidance_DataPrepChecklist.pdf)

> To Add:
- Donders
- EU Brain data sharing community
- Open Brain Consent
- http://www.sussex.ac.uk/psychology/brainshare/index

The data sharing logo was created by Adrien Coquet for [the Noun Project](https://thenounproject.com)

Materials within this repository were developed with the support of the [Open Life Sciences (OLS) Mentoring Program](https://openlifesci.org), with the kind assistance of [Naomi Penfold](https://github.com/npscience).

Other resources have also been developed following example materials developed by [The Turing Way](https://the-turing-way.netlify.app/welcome).

`The Turing Way Community, Becky Arnold, Louise Bowler, Sarah Gibson, Patricia Herterich, Rosie Higman, … Kirstie Whitaker. (2019, March 25). The Turing Way: A Handbook for Reproducible Data Science. Zenodo http://doi.org/10.5281/zenodo.3233853.`


# Citing this repository

**Please cite this repository as (authors listed alphabetically):**

Christoph Arthofer, Alon Baram, Stuart Clare, Michiel Cottaar, Matthijs H. S. de Buck, Cassandra D. Gould van Praag, Ludovica Griffanti, Clare E. Mackay, Duncan Mortimer, Duncan Smith, Mathew South, Benjamin C. Tendler, Mats W. J. van Es, Jessica Walsh, Tom Whyntie. (2022. WIN Open Neuroimaging Community: Data Sharing Decision Tree. Zenodo. 10.5281/zenodo.58261835785714

## Author Contributions (credit):

- Conceptualization: Stuart Clare, Cassandra D. Gould van Praag, and Clare E. Mackay. 
- Data curation: Cassandra D. Gould van Praag. 
- Formal analysis: Cassandra D. Gould van Praag. 
- Funding acquisition: Stuart Clare and Clare E. Mackay. 
- Investigation: Cassandra D. Gould van Praag. 
- Methodology: Stuart Clare, Cassandra D. Gould van Praag, and Clare E. Mackay. 
- Project administration: Stuart Clare, Cassandra D. Gould van Praag, and Clare E. Mackay. 
- Resources: Cassandra D. Gould van Praag. 
- Software: Cassandra D. Gould van Praag. 
- Supervision: Stuart Clare, Cassandra D. Gould van Praag, and Clare E. Mackay. 
- Visualization: Cassandra D. Gould van Praag. 
- Writing - original draft: Cassandra D. Gould van Praag. 
- Writing - review & editing: Christoph Arthofer, Alon Baram, Stuart Clare, Michiel Cottaar, Matthijs H. S. de Buck, Cassandra D. Gould van Praag, Ludovica Griffanti, Clare E. Mackay, Duncan Mortimer, Duncan Smith, Mathew South, Benjamin C. Tendler, Mats W. J. van Es, Jessica Walsh, and Tom Whyntie.

This resource is released under a [CC-BY-4.0 license](LICENSE.md)
