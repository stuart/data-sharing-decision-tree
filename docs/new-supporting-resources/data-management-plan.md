# Data management plans incorporating the Open WIN data sharing infrastructure

<mark> Yellow highlight indicates these details are to be finalised or confirmed. Please [contact us](#contact-us) if your project will make use of any of these features.</mark>

Data management plans are an early stage of project planning which are a required detail for some funding applications. In all cases, however, that are a good practice exercise to help you consider the logistical and technical specifics of managing the data you acquire in a secure and sustainable way.

The below information addresses data sharing using the Open WIN data sharing infrastructure, as it relates to the [BBSRC data policy](https://bbsrc.ukri.org/documents/data-sharing-policy-pdf/). This text may be used (with a [citation](#how-to-cite-this-material)) and incorporated into your own data management plans. Information aligned with the requirements for other grant awarding bodies will be created and shared as required.

## BBSRC data management plans

### Data areas and data types
Funding body requirement: Describe the volume, type and content of data that will be generated e.g. experimental measurements, models, records and images;

<mark>Note it has not been determined how the digital brain bank will sit alongside the Open WIN data sharing infrastructure. As new infrastructure is primarily designed to handle issues relating to human participant privacy, the digital brain bank may be a more appropriate repository for non-human data. As such the below refers to **in vivo human data only**.</mark>

The Open WIN Data Sharing Infrastructure is primarily designed to handle complex imaging data (MRI and MEG/EEG). Investigations using these tools often acquire data of other types as listed below. To ensure fidelity and completeness of data storage, all data types acquired in a single study will be stored and shared under project specific directories.

Data types handled by the Open WIN data sharing infrastructure:
- Magnetic resonance imaging (MRI), including function, structural, quantitative, diffusion weighted, or other forms which can be acquired with an MRI scanner
- Magnetic resonance spectroscopy (MRS)
- Magnetoencephalography (MEG)
- Electroencephalography (EEG)
- <mark>Digital histology</mark>
- Imaging deriverd phenotypes (IDPs)
- <mark>Genetic - probably need to link to another resource for full genomes, otherwise we could store genotypes?</mark>
- Behavioural data, such as reaction times or accuracy scores
- Physiological data, such are pupilometry, photoplethysmography (PPG) / pulse oximetry, respiratory, electrocardiography (ECG)
- Questionnaire data (symtomatic and lifestyle),
- Diagnostic information

### Standards and metadata
Funding body requirement: The standards and methodologies that will be adopted for data collection and management, and why these have been selected;

Unprocessed data will be stored in the format it is acquired (e.g. dicom for MRI). We encourage processed data to be stored and shared in accordance with community standards for improved operability.

The data standards we promote for each data type are listed below:
- MRI: [Brain Imaging Data Structure (BIDS)](https://bids.neuroimaging.io).
- Magnetic resonance spectroscopy (MRS): <mark>BEP in development?</mark>
- Magnetoencephalography (MEG): [BIDS-BEP008](https://bids.neuroimaging.io/get_involved.html#completed-beps) or [Neurodata Without Boarders (NWB)](https://www.nwb.org/)
- Electroencephalography (EEG): [BIDS-BEP006](https://bids.neuroimaging.io/get_involved.html#completed-beps) or [Neurodata Without Boarders (NWB)](https://www.nwb.org/)
- <mark>Digital histology</mark>
- <mark>Imaging deriverd phenotypes (IDPs)</mark>
- Genetic: [BIDS-BEP018](https://bids.neuroimaging.io/get_involved.html#completed-beps)
- <mark>Physiological data</mark>
- <mark>Diagnostic information</mark>

WIN is an active contributor to the development of these data standards (e.g. BEP001, BEP005, BEP025, BEP031) and works to support our researchers in the integration of such standards in their research practice

#### MR acquisition protocols
A crucial source of metadata for MRI studies is the acquisition parameters which were applied at source. These parameters can be captured and shared in their entirety using the [Open WIN MR Protocols Database](https://open.win.ox.ac.uk/protocols/). This database can be used to store and communicate image acquisition metadata. Each deposited protocol is version controlled to ensure transparency and reproducibility, and contains all information necessary to reproduce that sequence on an appropriately licensed scanner. This resource allows WIN researchers to have a permanent and full record of their data acquisition methodology. These protocols can be made available for sharing with external users via a web interface and with a digital object identifier to ensure that the developers of the protocol are attributed. **you might want to promise say something nice like “all new sequences developed using the requested technology will be made available with open access for non-commercial research."

### Relationship to other data available in public repositories;
Funding body requirement: How will the data shared via the Open WIN data portal complement other open data sources?

<mark>How will the Open WIN data portal complement Biobank, digital brainbank, for example?</mark>

### Secondary use
<mark>Something here about how we promote the reuse of data via appropriate doi, licensing, contact, authorship agreements.</mark> - further intended and/or foreseeable research uses for the completed dataset(s);

### Methods for data sharing
Funding body requirement: Planned mechanisms for making these data available, e.g. through deposition in existing public databases or on request, including access mechanisms where appropriate;

WIN has invested in the development of new data sharing infrastructure to fulfil the unique requirements of our discipline and legislature (notably GDPR). This infrastructure is the first of its kind in the UK. Development is supported by a programme of training and behaviour change activities to embed a culture of sharing within our team. Human imaging data will be made available for sharing via our Open Data Repository and non-human animal data will be shared via the Digital Brain Bank. In all cases the Principal Investigators will be able to determine the appropriate level of external access to their data (in cases where all data cannot be made immediately public), to control release in line with BBSRC guidelines. Users of the repository will have access to integrated quality control assessments to ensure that all data which is released comes is accompanied by precise quality descriptors.

Other outputs generated through receipt of this award may include research software or analysis routines. WIN recommends and supports the development of these products using free and open source languages, and their distribution with appropriate documentation and training to enable effective reuse. WIN has significant experience in this area, being the home of the FMRIB Software Library (FSL), which has been free for non-commercial use since its inception. The FSL package is now one of the leading MRI analysis tools in the sectors, and continue to grow in the diversity of data types it can be applied to. WIN recommends and supports sharing of research software through our self-hosted GitLab instance. This is supported by training in best practice in digital archiving and licensing to ensure that our users receive full and appropriate recognition for their work.

### Proprietary data
Funding body requirement: Any restrictions on data sharing due to the need to protect proprietary or patentable data;

Teams are encouraged to explore the full economic potential of their research outputs, however Heads of Department are authorised to support sharing under appropriate open source licenses **Note I would like to get this authorisation moved to Heidi, and have a blanket statement of approval in principle. Where economic potential exists, WIN favours models where outputs are made freely available for non-commercial use **Note this isn’t an explicit WIN statement, just a general position the OUI have discussed where we get to retain some commercial value.

### Timescales for public release of the data:
Funding body requirement: Timescales for data sharing will be influenced by the nature of the data but it is expected that timely release would generally be no later than the release through publication of the main findings and should be in-line with established best practice in the field. BBSRC expects research data generated as a result of BBSRC support to be made available with as few restrictions as possible in a timely and responsible manner to the scientific community for subsequent research. Timescales for data sharing will be influenced by the nature of the data but it is expected that timely release would generally be no later than the release through publication of the main findings and should be in-line with established best practice in the field.

Our infrastructure is being designed to support the level of access and frequency of release as prescribed by individual research teams (e.g. with embargoes or near-instantaneously). There is no intention to establish a WIN-wide policy on timescales of release.

## How to cite this material

<mark>TBC</mark>

## Where to get help

## Contact us
