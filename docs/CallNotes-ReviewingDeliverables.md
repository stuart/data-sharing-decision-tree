# Data Sharing Decision Tree - Reviewing Dleiverables

-----

**Important information**
**Where**: Teams
**When**: 13/01/2022

**Material we will be reviewing**:
- [Decision tree](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree/-/blob/master/docs/decision-tree.md)
- [Appendices](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree/-/blob/master/docs/decision-tree-appendicies.md)
- [Call Summaries and Actions]

-----
## Participants

1. Cassandra Gould van Praag
2. Stuart Clare
3. Clare Mackay

Break down the content into what we can put deliverables against and make a plan for what we cannot yet deliver.

This will never be finished, because there are so many eventualities.

Don't want to wait until everything is finished before we give people something!

**We need to reduce the problem space**

The project is too much to consider as one deliverable
- Can I share my data?
- How do I share my data?

Could first make the "can I" tool, and note that we are working on the "how do I"

- Can i? aka "am I allowed to"
  - process 1 & 2
  - inc "is it de-identified" high level questions, but not details of how you do it
- How
  - how to actually do it
  - process 3-5

- Want to have some sense that things can be "ticked off"

For each process:
- What is it trying to achieved
- How will I know when I have done it
- **Identify what is in and out of scope of what we're trying to do, what we have and haven't covered.**

- [x] Add "check with funder" to decision tree (contracts) => "Have you considered legal and contractual issues"

We're facilitating the bit we can, but ultimately the responsibility rests with PI.

We often get stuck on finding out who can make the decision to "sign off" on something. Indicate when you do and don't need to get sign off. Say that we can "advise" but not sign off. Clearly sign-post to what group can sign-off.

Research data security training is out of scope.

"Have you included enough metadata to make it FAIR" - begs the question, what is enough metadata? "We are working on the answers and can signpost.

Write out the "can I questions"
- include some context and definitions - simple steps

"Can I share my data in a way which ensures I will receive attribution?" i.e. license

**Turn all of it into the sub-headings**
- Why should I? Incentive, motivation.
- Can I?
- How do I?
- When should I?

Format the questions with collapsable text, like this:

<details>
<summary>Has my participant consented?</summary>
Your consent form my contain a separate question about open data sharing (question 4 on AP17). Has your participant initialed this box?
</details>

<details>
<summary>Did my ethics include data sharing?</summary>
Did you mention data sharing in online repositories in your ethics application? If you used AP17, this is covered as ...
</details>

## Actions
- [ ] Cass to format decision tree (or first processes) as questions under the "Can i" etc headings.
- [ ] Give some brief context and definitions on each
- [ ] Check scope on all sections
- [ ] Meet next week with Clare to review
