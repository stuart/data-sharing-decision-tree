# Open data decision tree

## Overview
We have divided the data sharing process into five stages. Each stage should be reviewed in turn to see what path your project should follow.
![](../img/process-overview-key.png)

### Start and Stop
In each process there is a "start" point, and in some cases a hard "stop". A "stop" is reached where the project has not achieved the mandatory stages as required by University policy or GDPR.

### Navigating through the processes
Each process links to the next in the "Go to next process" box. Not all data types will follow all five steps sequentially; some project may skips as indicated by following the tree according to the answers given.

### Recommended and mandatory steps
Each process has both "recommended" and "mandatory" steps. Mandatory steps fulfil your requirements according to University and GDPR. Following the recommended steps brings your project into alignment with the growing consensus for data sharing according to the [FAIR
principles](https://www.go-fair.org/fair-principles/)


## Whole thing iframe

<html><body>
<figure class="video_container">
<iframe frameborder="0" style="width:100%;height:1800px;" src="https://viewer.diagrams.net/?highlight=0000ff&edit=_blank&layers=1&nav=1&title=WIN-data-sharing-decision-tree-draft2-8.drawio#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1ljAORgTRcjE6nzqPSA1ZUX2yAESBAcvx%26export%3Ddownload"></iframe>
</figure>
</html></body>

<!-- ![](../img/WIN-data-sharing-decision-tree-draft2-8.svg) -->

<!-- <a name="ethics-rdm"></a>
## Process 1 - Ethics, consent and data management
*to-do: explanatory text here*
![](../img/p1.png)

<a name="protected"></a>
## Process 2 - Protected features of the data
*to-do: explanatory text here*
![](../img/p2.png)

<a name="deidentify"></a>
## Process 3 - Deidentification
*to-do: explanatory text here*

> See also:
> - [Appendix 1 - DICOM headers to be scrubbed](./decision-tree-appendicies.md#appendix-1-dicom-headers-to-be-scrubbed)
> - [Appendix 2 - BIDS compliance](./decision-tree-appendicies.md#appendix-2-bids-compliance)
> - [Appendix 3 - Scrubbing .json files](./decision-tree-appendicies.md#appendix-3-scrubbing-json-files)
> - [Appendix 4 - Quality control](./decision-tree-appendicies.md#appendix-4-quality-control)
> - [Appendix 5 - Defacing](./decision-tree-appendicies.md#appendix-5-defacing)


![](../img/p3.png)

<a name="metadata"></a>
## Process 4 - Metadata
*to-do: explanatory text here*

> See also:
> - [Appendix 2 - BIDS compliance](./decision-tree-appendicies.md#appendix-2-bids-compliance)

![](../img/p4.png)

<a name="sharing-attribution"></a>
## Process 5 - Sharing and attribution
*to-do: explanatory text here*

> See also:
> - [Appendix 6 - Upload to XNAT](./decision-tree-appendicies.md#appendix-6-upload-to-xnat)
> - [Appendix 7 - Data freeze](./decision-tree-appendicies.md#appendix-7-data-freeze)
> - [Appendix 8 - Digital object identifiers (DOI)](./decision-tree-appendicies.md#appendix-8-digital-object-identifiers-doi)
> - [Appendix 9 - Data usage agreement (DUA)](./decision-tree-appendicies.md#appendix-9-data-usage-agreement-dua)

![](../img/p5.png)

# References
[DPIA screening and DPIA/DPA templates]
(https://compliance.admin.ox.ac.uk/data-protection-forms#collapse1091641)

Jones and Ford, 2018. "Privacy, confidentiality and practicalities in data linkage. National Statistical Quality Review". [https://cronfa.swan.ac.uk/Record/cronfa53688](https://cronfa.swan.ac.uk/Record/cronfa53688)

Harron et al, 2017. "Challenges in administrative data linkage for research". Big Data & Society. December 2017. [doi:10.1177/2053951717745678](https://journals.sagepub.com/doi/10.1177/2053951717745678)

[Open Brain Consent GDPR Edition](https://open-brain-consent.readthedocs.io/en/stable/gdpr/index.html)

[CUREC Best Practice Guidance (BPG) 09 "Data collection, protection and management".](https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf) -->
