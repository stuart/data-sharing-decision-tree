# Data Sharing Decision Tree - Refactored to questions

<!-- <details>
<summary>Question?</summary><br>
details
</details> -->

![data sharing questions](../img/sharing-questions_w800px.png)


## Why should I share my data?

<details>
 <summary>To create a citable research output</summary><br>

 Collecting and curating your research data is often a huge undertaking which requires significant skill and expertise. You deserve to receive credit for the data you have created as an independent research object, over and above any publish manuscript. We will help you to share your data in a way that it can exist as a citable research object, on your CV and the digital realm.
</details>


<details>
 <summary>Publisher requirement</summary><br>

 Many publishers will now require you to share data, so your research findings can be verified. Our infrastructure will enable you to fulfil these requirements. Caution: Don't wait until you are about to publish before thinking about data sharing! It is much easier to share data if you have built in sharing as part of your study.
</details>


<details>
 <summary>Funders want to make the most of their investment</summary><br>

 I'm adding some text here...

 <mark>TBC</mark>
</details>


<details>
 <summary>Sharing is becoming the norm</summary><br>

 <mark>TBC</mark>
</details>



<details>
 <summary>A sustainable platform and archive of your data</summary><br>

 <mark>TBC</mark>
</details>



<details>
 <summary>Speeds up translation</summary><br>

 <mark>TBC</mark>
</details>


## Can I share my data?

Use this [checklist](./checklist-cani.md) to keep track of your answers to the questions below.

<!-- -->

<details>
 <summary>Is sharing restricted under Intellectual Properties rights?</summary><br>

 <details>
  <summary>Has your funder or industry partner approved data sharing?</summary><br>

  <mark>TBC</mark>
 </details>

 <details>
  <summary>Have you investigated commercial potential of your data?</summary><br>

  <mark>TBC</mark>
 </details>

</details>

<!-- -->

<details>
 <summary>Are you sharing data acquired from living humans?</summary><br>

 GDPR restrictions relate only to data acquired from living humans.

 **Non-human** data are not required to be de-identified. Consider sharing your data [Digital Brain Bank](https://open.win.ox.ac.uk/DigitalBrainBank/#/)

 **Ex vivo human data*** should be treated in accordance with the requirements of the Common Law Duty of Confidentiality. You should also be aware of the possibility of living individuals (for example relatives of the deceased) being identified in this information, which would then need to be treated in line with GDPR personal information. Please review the [HRA Decision Tool for principles for handling data from deceased human participants](http://www.hra-decisiontools.org.uk/consent/principles-deceased.html). Consider sharing your data [Digital Brain Bank](https://open.win.ox.ac.uk/DigitalBrainBank/#/).
</details>

<details>
 <summary>Have you considered data governance with respect to sharing?</summary><br>

 <details>
  <summary>Have you discussed open data sharing in your data management plan?</summary><br>

  Creating a data management plans helps you plan how you will manage the data acquired during your project by considering the type of data you are producing, who needs to access it and accordingly how it is stored. They are a mandatory part of some grant applications, but they are also a useful exercise for smaller projects which don't require separate funding.

  Find out more about how the University can [support you in creating a data management plan](https://researchdata.ox.ac.uk/home/managing-your-data-at-oxford/data-management-planning/)

  Review a [MRC format WIN specific example data management plan (pre data sharing infrastructure)](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/_layouts/15/WopiFrame.aspx?sourcedoc=/sites/NDCN/FMRIB/IT/Documents/WIN%20Centre%20-%20Data%20Management%20Plan%20MRC%20Template.docx&action=default&DefaultItemOpen=1)

  Review the [BBSRC requirements for data management plans in relation to data sharing](../new-supporting-resources/data-management-plan)
 </details>

 <details>
  <summary>Have you completed a Data Protection Impact Assessment (DPIA) screening form which references data sharing?</summary><br>

  All studies which collect new or re-use existing data must be assessed for risks of a data breach. This risk is assessed using a [Data Protection Impact Assessment (DPIA) Screening form](https://compliance.admin.ox.ac.uk/data-protection-forms#collapse1091641)

  Note for the purposes of the DPIA Screening, human imaging data may be considered "biometric data": "personal data resulting from specific technical processing relating to the physical, physiological, or behavioural characteristics of a natural person, which allows or confirms the unique identification of that natural person, such as facial images or fingerprint data."
 </details>

</details>

<!-- -->

<details>
 <summary>Do you have ethical approvals in place?</summary><br>

 <details>
  <summary>Have you described data sharing in your ethics application?</summary><br>

  Your ethics application and participant information sheet should minimally refer to the sharing of data with colleagues outside of the University. Ideally, you should include the possibility of sharing "deidentified data in online databases". If you have collected MRI data under [CUREC Approved Procedure 17](https://researchsupport.admin.ox.ac.uk/governance/ethics/resources/ap#collapse397171) (version 6.0+) statement fulfilling this requirement will be included.
 </details>

 <details>
  <summary>Has your participant consented to data sharing?</summary><br>

  Many consent forms have a separate section or box to indicate the participant is aware of your data sharing plans (box 4 on the [Approved Procedure 17 consent form](https://researchsupport.admin.ox.ac.uk/governance/ethics/resources/ap#collapse397171)). Has your participant indicated they have agreed to data sharing as you have described?
 </details>

</details>

<!-- -->

<details>
 <summary>Have you de-identified your data?</summary><br>

 <details>
  <summary>Have you removed any "direct identifiers" in your data?</summary><br>

  <mark>Direct identifiers include...</mark>
 </details>

 <details>
  <summary>Are your imaging data in participant space?</summary><br>

  Participant space (cortical structure) is unique to an individual. Avoid sharing data which is in participant space where possible. If it is preferable to share data in participant space, ensure other features described below are redacted as far as possible and appropriate for your analysis.
 </details>


 <details>
  <summary>Have your Participant IDs been protected?</summary><br>

  Are any keys which link researcher generated Participant IDs and WIN generated Scan ID Participant IDs to GDPR "special category" data (names, contact information, consent documentation etc.) held in a facility which is surrounded by a suitable regime of controls and safeguards to prevent data breaches and misuse (Jones and Ford, 2018)? In practice, this is achieved by following CUREC BPG 09, with data only held on a approved shared drive (Departmental or One Drive), or a device with whole disk encryption.

  The linkage key must be "stored separately from" ([CUREC BPG 09](https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf)) special category and research data, It must not be shared with research data except in critical circumstances. The validity of requests for access to the linkage key should be assessed on an individual basis by the responsible data controller (usually the Principle Investigator).
 </details>


 <details>
  <summary>Have "indirect identifiers" such as age, gender, handedness or disease status been protected?</summary><br>

  Consider combining "indirect identifiers" ([CUREC BPG 09](https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf)) into bins such that no participant can be uniquely identified. Ideally bins should contain n > 5 participants.
 </details>

 <details>
  <summary>Have unique dicom fields been scrubbed?</summary><br>

  If you are sharing dicom data, you should scrub the dicom headers of all identifiable and unique fields.
 </details>

 <details>
  <summary>Have unique fields in .json sidecar files been scrubbed?</summary><br>

  If you are sharing nifti data with json sidecar files, you should scrub the .json files of all identifiable and unique fields.
 </details>

 <details>
  <summary>Have images been defaced?</summary><br>

  If you are sharing data with facial features, have these images been defaced and assessed for the quality of the defacing? Consider using fsl-deface.
 </details>

 <details>
  <summary>Have you conducted and prepared to share a quality control analysis?</summary><br>

  It is good practice to share a quality control (QC) analysis. Consider using mriqc.
 </details>

</details>

<!-- -->

<details>
 <summary>Have you prepared the data according to community standards?</summary><br>

 <mark>BIDS, BEPs</mark>
</details>

<!--  -->

<details>
 <summary>Have you prepared the metadata to make your data FAIR?</summary><br>

 <details>
  <summary>Are behavioural and clinical covariates appropriately described?</summary><br>

  Measured results for each participant should be provided in a single file containing all covariates, in appropriately [machine readable structure](https://en.wikipedia.org/wiki/Machine-readable_data).
  Covariates should be accompanied by a data dictionary which describes each of the variables included, how they were derived and the source data where possible.
 </details>

 <details>
  <summary>Are you able to share the image acquisition protocol?</summary><br>

  Consider adding the MR protocol and scanning procedure documents to the [MR Protocols database](https://open.win.ox.ac.uk/protocols/). Add a link to your database entry in your shared data.
 </details>

 <details>
  <summary>Has the experimental protocol been described and made ready to share with the data?</summary><br>

  Minimal experimental detail is as described in the dataset_descriptor.json file which is generated during BIDS conversion.
 </details>

</details>

<!--  -->

<details>
 <summary>Can access to your data be restricted?</summary><br>

 <details>
  <summary>Can you create a "reviewer only" link?</summary><br>

  In some cases you may wish to make your data available only to a reviewer before making it available for wider release. Is this possible with your intended repository?
 </details>

 <details>
  <summary>Can you restrict access to bonafide researchers only?</summary><br>

  Given a the need for responsible reuse of your data, it may be wise to restrict re-use to those individuals who are likely to have a genuine research interest. Can your intended repository restrict access to allow only bonafide researchers, for example by institutional email verification, or an [ORCID ID](https://orcid.org)?
 </details>

</details>

<!-- -->

<details>
 <summary>Can you ensure that you are appropriately acknowledged when your data are reused?</summary><br>

 <details>
  <summary>Can you create a doi for your data?</summary><br>

  Does the tool you are using to share your data allow you to create a citable digital object identifier (doi) for the exact version of your data you are sharing? This doi can be used by others to reference your data.
 </details>

 <details>
  <summary>Can you select a license which requires attribution?</summary><br>

  Your data is a significant intellectual output, and you deserve to be recognised for it if your output is reused. We recommend using a repository where you can apply a license for reuse which necessitates attribution, for example [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/). You may additionally like to apply a license which restricts commercial use (for example [CC-BY-NC-4.0](https://creativecommons.org/licenses/by-nc/4.0/legalcode)), allowing commercial use to be negotiated by the University.
 </details>

</details>

<!-- -->

<details>
 <summary>Would you like to impose custom terms around the reuse of your data?</summary><br>

 <details>
   <summary>Would you like to impose requirements for authorship?</summary><br>

   You may wish to stipulate that you are contacted to discuss authorship and further contributions if your data are reused. Alternatively you may wish to stipulate that you are not included as an author on any reuse of the data. Is it possible to impose such requirements with your intended repository?
  </details>

  <details>
   <summary>Would you like to impose restrictions on resharing?</summary><br>

   You may wish to stipulate that users of your data do not share it any further once they have acquired a copy. Is it possible to impose this requirement with your intended repository?
  </details>

  <details>
   <summary>Would you like to explicitly prohibit attempts to reidentify participants in your data?</summary><br>

   Given that many types of imaging can not be made fully anonymous, it may be wise to include an additional legal restriction which explicitly prohibits attempts to reidentify your participants, for example via linkage to other public sphere or experimental data. Is it possible to impose such requirements with your intended repository?
  </details>

  <details>
   <summary>Would you like to add any funder requirements in the reuse of your data?</summary><br>

   Some funders may require acknowledgement in perpetuity for data generated with their funds. Is it possible to impose such requirements with your intended repository?
  </details>

</details>



## How do I share my data?

<!-- <details>
<summary>Question?</summary><br>
details
</details> -->

<mark>TBC</mark>

## When should I think about sharing my data?

<mark>TBC</mark>
